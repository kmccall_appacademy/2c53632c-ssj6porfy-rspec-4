class Dictionary
  attr_accessor :entries

  def initialize(entry = {})
    @entries = entry
  end

  def add(new_entry)
    if new_entry.is_a?(Hash)
      @entries = @entries.merge(new_entry)
    else
      @entries[new_entry] = nil
    end
  end

  def include?(key)
    @entries.keys.include?(key)
  end

  def keywords
    @entries.keys.sort
  end

  def find(key)
    partial_length = key.length - 1
    result_hash = {}
    @entries.keys.each do |keyword|
      if keyword == key || keyword[0..partial_length] == key
        result_hash[keyword] = @entries[keyword]
      end
    end
    result_hash
  end

  def printable
    result = []
    keywords.each do |key|
      result << "[#{key}] \"#{@entries[key]}\""
    end
    result.join("\n")
  end
end
