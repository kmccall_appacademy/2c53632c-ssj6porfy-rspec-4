class Book
  attr_accessor :title

  def title
    words = @title.split(' ')
    excluded_words = %w[in a and the an of]
    result = words.map do |word|
      excluded_words.include?(word) ? word : word.capitalize
    end.join(' ')
    result[0] = result[0].upcase
    result
  end
end
