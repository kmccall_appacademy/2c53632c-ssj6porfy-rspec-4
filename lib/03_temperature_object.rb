class Temperature

  def initialize(temp_types = {})
    if temp_types[:f]
      @fahrenheit = temp_types[:f]
    else
      @celsius = temp_types[:c]
    end
  end

  def in_fahrenheit
    @celsius.nil? ? @fahrenheit : self.class.ctof(@celsius)
  end

  def in_celsius
    @fahrenheit.nil? ? @celsius : self.class.ftoc(@fahrenheit)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def fahrenheit=(temperature)
    @fahrenheit = temperature
  end

  def celsius=(temperature)
    @celsius = temperature
  end

  def self.ctof(temperature)
    (temperature.to_f * (9.to_f / 5.to_f)).to_f + 32
  end

  def self.ftoc(temperature)
    (temperature.to_f - 32) * (5.to_f / 9.to_f).to_f
  end
end

class Fahrenheit < Temperature
  def initialize(temperature)
    self.fahrenheit = temperature
  end
end

class Celsius < Temperature
  def initialize(temperature)
    self.celsius = temperature
  end
end
