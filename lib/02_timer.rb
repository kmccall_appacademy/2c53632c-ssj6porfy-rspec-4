class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    time_arr = [(@seconds / 60 / 60) % 60,(@seconds / 60) % 60,@seconds % 60]
    result = time_arr.map(&:to_s)
    result.map { |time| time.length == 2 ? time : '0' + time }.join(':')
  end

end
